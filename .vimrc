" Make CTRL + d delete word
imap <C-Del> <C-O>dw
imap <C-D> X<Esc>lbce

" Make Shift + arrows move lines
nnoremap <S-Up> :m-2<CR>
nnoremap <S-Down> :m+<CR>
inoremap <S-Up> <Esc>:m-2<CR>
inoremap <S-Down> <Esc>:m+<CR>

" Shift tab for command mode
nnoremap <S-Tab> <<
" Shift tab for insert mode
inoremap <S-Tab> <C-d>

" Fix deletion problem
nnoremap x "_x
nnoremap d "_d
nnoremap D "_D
vnoremap d "_d

nnoremap <leader>d ""d
nnoremap <leader>D ""D
vnoremap <leader>d ""d

" Highlighting
set hlsearch

" Press Space to turn off highlighting and clear any message already
" displayed.
nnoremap <silent> <Space> :nohlsearch<Bar>:echo<CR>

" Saving file
noremap <silent> <C-S>          :update<CR>
vnoremap <silent> <C-S>         <C-C>:update<CR>
inoremap <silent> <C-S>         <C-O>:update<CR>

" Return to last edit position when opening files
augroup last_edit
  autocmd!
    autocmd BufReadPost *
       \ if line("'\"") > 0 && line("'\"") <= line("$") |
       \   exe "normal! g`\"" |
       \ endif
augroup END

" ALE
let g:ale_linters = {'php': ['php', 'langserver']}
let g:ale_php_langserver_executable = expand('C:\Users\jaho\AppData\Roaming\Composer\vendor\bin\php-language-server.php')
"let g:ale_completion_enabled = 1
let g:ale_php_langserver_use_global = 1
let g:ale_completion_autoimport = 1
set omnifunc=ale#completion#OmniFunc

" Go 
let mapleader=","
au FileType go nmap <leader>r <Plug>(go-run)
au FileType go nmap <leader>b <Plug>(go-build)
au FileType go nmap <leader>t <Plug>(go-test)
au FileType go nmap <leader>c <Plug>(go-coverage)
" -----------------------------------------------------------------------------
"   Plugins
"  -----------------------------------------------------------------------------
call plug#begin('~/.vim/plugged')

Plug 'rust-lang/rust.vim'
Plug 'vim-scripts/AutoComplPop'
Plug 'morhetz/gruvbox'
Plug 'mhinz/vim-signify'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-commentary'
Plug 'vim-airline/vim-airline'
Plug 'https://github.com/AndrewRadev/splitjoin.vim'
Plug 'dense-analysis/ale'
Plug 'rust-lang/rls'
Plug 'nvie/vim-flake8'
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }

call plug#end()

" -----------------------------------------------------------------------------
"   Color settings
"  -----------------------------------------------------------------------------
" Enable 24-bit true colors if your terminal supports it.
if (has("termguicolors"))
" https://github.com/vim/vim/issues/993#issuecomment-255651605
	let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
	let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
    set termguicolors
endif

syntax on
colorscheme gruvbox
set background=dark
set complete+=kspell
set tabstop=4
set softtabstop=4 expandtab
set number
set hidden
set belloff=all

" Use gcc to comment out a line (takes a count), gc to comment out the target
" of a motion (for example, gcap to comment out a paragraph), gc in visual
" mode to comment out the selection, and gc in operator pending mode to target
" a comment. You can also use it as a command, either with a range like
" :7,17Commentary, or as part of a :global invocation like with
" :g/TODO/Commentary
set hidden
